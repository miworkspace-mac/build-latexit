#!/bin/bash

NEWLOC=`curl -L "https://www.chachatelier.fr/latexit/latexit-downloads.php" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36' 2>/dev/null | grep .dmg | head -1 | /usr/local/bin/htmlq -a href a `


if [ "x${NEWLOC}" != "x" ]; then
	echo "https://www.chachatelier.fr/latexit/"${NEWLOC}
fi