#!/bin/bash

DIRECTORY="/Applications/TeX"

if [ ! -d "$DIRECTORY" ]; then
  /bin/mkdir -p -m 775 /Applications/TeX
fi

